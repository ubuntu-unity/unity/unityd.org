---
publishDate: 'Nov 11 2022'
title: 'Unity, now on Arch'
description: "Attention Arch users! The wait is over - Unity 7.6 is finally here, and it's ready to rock your desktop. So go ahead and update your system, because it's time to get your Unity on!"
image: '~/assets/images/blog/unity-for-arch.png'
category: 'Distributions'
author: 'Rudra Saraswat'
tags: [arch, distros]
---

Unity 7.6 is now available for Arch Linux! We've ported all of the Unity packages in the Ubuntu repositories, including `unity-control-center`, `unity-settings-daemon`, `compiz` with the Ubuntu patches, `nux`, `unity-greeter`  and `unity-tweak-tool` among others.

**Installing Unity on Arch Linux**
1. Add the repository key to the pacman keyring:

```sh
curl https://unity.ruds.io/repo.key | sudo pacman-key --add -
sudo pacman-key --lsign-key 3FB6809130A5DB7F
```

2. Insert the following lines right above `[community]` in /etc/pacman.conf:

```sh
[arch-unity]
SigLevel = Required DatabaseOptional
Server = https://unity.ruds.io/arch-unity
```

3. Install Unity (no helper needs to be installed):

```sh
sudo sh -c "pacman -Syyu; pacman -S unity-meta"
```

4. Switch to LightDM as the default display manager after disabling the current display manager:

```sh
sudo systemctl enable --now lightdm
```

5. Voila! You should now see Unity in the session list, and be able to log into it.

**Notes**
* Some packages, such as `appmenu-gtk-module` are known to cause some apps to break under Unity.

**Contributing**

You can help contribute to the packaging at https://gitlab.com/Unity-for-Arch.

**Credits:** This port is based on an earlier effort, Unity-for-Arch by chenxiaolong (maintained from 2011-2016). I would also like to thank 3v1n0, mitya57 and c4pp4 for their help in this journey.