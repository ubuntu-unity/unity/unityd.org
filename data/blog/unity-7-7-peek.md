---
publishDate: 'Dec 25 2022'
title: 'Unity 7.7: a sneak peek and call for feedback'
description: "Get an early preview of Unity 7.7 and help shape its development by sharing your feedback. Your input is valuable and will be taken into consideration as we work to improve and enhance Unity 7.7."
image: '~/assets/images/blog/unity-7.7-peek/banner.png'
category: 'Releases'
author: 'Rudra Saraswat'
tags: [unity, release, sneakpeek, feedback]
---

Attention all Unity aficionados! We've been slaving away in the code mines, bringing back some old-school UI concepts from the days of Canonical maintenance. And let me tell you, the results are nothing short of spectacular. Just take a peek at these screenshots - they'll have you doing a little UI dance in no time. So, without further ado, let's dive into the nitty-gritty of these new updates.

## UWidgets

<img src="/blog-assets/unity-7.7-peek/uwidgets.png"/>

We've been working our widget-making fingers to the bone to bring you a brand new plugin system for Unity: UWidgets. This new system allows you to display widgets (kind of like Cinnamon desklets) written in Python on top of your Unity7 background. And the best part? You can easily download and copy them to your `~/.local/share/unity/widgets` folder to install them. We've already made a bunch of awesome widgets – a clock, a system monitor that links to Stacer, a Spotify widget that lets you tap to play or pause music, and a widget that sets a random Unsplash wallpaper when clicked (check out the screenshot for a sneak peek).

But that's not all – UWidgets' libraries also allow widgets to modify Unity7's appearance and settings and do all sorts of fancy things: for example, it allows them to change the wallpaper or the position and other settings of the dock/launcher. Plus, many of its libraries can be used in existing apps written for Unity, to integrate with the desktop. I'm even thinking of using UWidgets to rewrite a lot of the code for unity-tweak-tool.

And the fun doesn't stop there – we'll be setting up a web store/repository for UWidgets, where you can either submit your own widgets, or download and try out all those amazing widgets on Unity 7.7. So go ahead and get ready to widget-ize your desktop – the possibilities are endless with UWidgets.

## Dash position

<img src="/blog-assets/unity-7.7-peek/dash.png"/>

Say goodbye to the old, boring Unity7 dash – it's time for a fresh, new dash to take its place! It's based on the design concepts being discussed for Unity7 right before Canonical dropped it. It's ready to revolutionize the way you use your computer, with its sleek design and intuitive navigation. So go ahead and bid farewell to the old dash – the new Unity7 dash is here to stay! (of course, subject to your feedback and approval)

## New welcome app

<img src="/blog-assets/unity-7.7-peek/welcome-app.png">

We've got some exciting news for you – a new welcome app is on the horizon, and it's written in Flutter. This app is based on the Welcome app prototype developed by the Ubuntu Flutter Community, and let's just say, it's going to be a real flutter-tastic addition to your desktop. But don't worry, it will still be available for all the distros supported by Unity, so everyone can enjoy a warm welcome on their desktop.

## Panel changes

<img src="/blog-assets/unity-7.7-peek/panel-light.png"/>
<img src="/blog-assets/unity-7.7-peek/panel-dark.png"/>

The new panel is slightly bigger, but it's a real show-panel! It looks a lot more visually-appealing, especially when you're using a light theme – it really panel-ishes in comparison to the old one. It's like night and day, or should I say, light and panel?

## Notifications

<img src="/blog-assets/unity-7.7-peek/notifications.png"/>

While the indicator-notification may have existed in the past, it was never given the spotlight it deserved – until now. That's right, Unity 7.7 is finally giving the indicator-notification its time to shine!

This is a massive improvement for usability, folks. No longer will you have to rely on your good old-fashioned memory to keep track of your notifications. The indicator-notification will be there for you, like a trusty sidekick (or rather, a notification-indicator).

## Everything else

In other news, we've:
* Replaced the launcher BFB with a half-transparent icon, similar to the Ubuntu Unity 21.04 launcher BFB
* Reduced default launcher icon size to 44

<img src="/blog-assets/unity-7.7-peek/unity-control-center.png"/>

* improved unity-control-center shell UI
* Reduced default panel opacity to 0.75

## Feedback

We value your opinion and would love to hear your thoughts on these new changes, especially on the Dash. Your feedback is crucial to helping us create the best possible experience for you. So please don't hesitate to share your thoughts with us on Twitter (**@ubuntu_unity**), Telegram (https://t.me/ubuntuunitydiscuss), and Discord (https://discord.gg/JZryxUMGZf). We're all ears!