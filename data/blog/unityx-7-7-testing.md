---
publishDate: 'Jan 11 2022'
title: 'UnityX 7.7: available for testing'
description: "Get an early preview of Unity 7.7 and help shape its development by sharing your feedback. Your input is valuable and will be taken into consideration as we work to improve and enhance Unity 7.7."
image: '~/assets/images/blog/unityx-7.7-testing/banner.png'
category: 'Releases'
author: 'Rudra Saraswat'
tags: [unityx, release, testing, feedback]
---

Greetings Unity lovers! As you're likely aware, we recently succeeded in running Unity 7.7 without Compiz. Based on this effort, we've created a new variant of Unity7. Presenting UnityX (superseding the previous effort with the same name), a new variant of Unity that retains all the features of Unity7, yet offers even more customization options (like using a window manager of your choice with Wayland support coming soon, replacing or even removing the panel etc). It doesn't even require any of Ubuntu's GTK patches. The look and feel of UnityX is akin to Unity7, but with added flexibility. You can install it from https://gitlab.com/ubuntu-unity/unity-x/unityx#manual-installation, until it's in the Arch/Gentoo/Manjaro/Ubuntu repos. Both Unity7 and UnityX will be shipping in the upcoming Ubuntu Unity and Manjaro Unity releases. We can't wait for you to try it out! Here are the features:

## Dash and Launcher

<img src="/blog-assets/unityx-7.7-testing/desktop.png"/>

The dash is not just visually identical to that of Unity 7.7, but also shares the same underlying code, resulting in a stable and reliable experience. You now have the added flexibility of adjusting the opacity of both the dash and the launcher through UnityX's configuration file.

Additionally, the launcher/dock has also been borrowed from Unity7 and it has been seamlessly integrated and works perfectly fine.

## The HUD

<img src="/blog-assets/unityx-7.7-testing/hud.png">

We have an all-new HUD based on Plotinus, and you can see it in action in this screenshot (and it opens with **Ctrl+Shift+P**). It supports a lot more apps than Unity7's HUD, and unlike Unity7's HUD, can be opened simultaneously in multiple apps.

## UnityX Control Center

<img src="/blog-assets/unityx-7.7-testing/unityx-control-center.png"/>

The System Settings app is a fork of that of Unity7, but supports changing most of UnityX's config.

For the true tech aficionados among us, a (read: novel) configuration protocol has been devised, as expounded upon in the subsection below, specifically tailored to the requirements of elite power users with an in-depth understanding of the intricacies of the **System™**.

## Advanced configuration

```yaml
general:
  opacity: 0.8
  hud_enabled: true
  lowgfx: false
  use_common_config: true

launcher:
  position: "left"

autostart:
  wm: "xfwm4"
  exec: ["xfce4-panel",
         "unity-settings-daemon",
         "nm-applet",
         "nemo-desktop",
         "unityx-indicator-session",
         "/usr/libexec/vala-panel/appmenu-registrar",
         "sleep 0.5; unityx-indicator-sound",
         "sleep 1; unityx-indicator-appearance",
         "sleep 1.5; unityx-power-manager",
         "blueman-applet"]
```
| |
| :-: |
| UnityX's default configuration (simplified) |

Introducing a new way to configure UnityX. You read that right, it is now possible to replace all of UnityX's components, including the window manager, panel and settings daemon! For instance, if you desire to integrate support for UWidgets, all you need to do is append **uwidgets-runner** to the bottom of the **exec** list.

To make modifications to the config, simply log into UnityX and edit the file located at **~/.local/share/unity/unityx.yaml**, which houses the complete default configuration.

## Everything else

In other news, the Unity7 appearance indicator has been ported to UnityX, and there's a new session indicator for UnityX. It's also possible to open files directly from the global menu when on the desktop (in the default config, from **vala-panel-appmenu**).

<img src="/blog-assets/unityx-7.7-testing/appearance_indicator.png">

## Feedback

We value your opinion and would love to hear your thoughts on UnityX. Your feedback is crucial to helping us create the best possible experience for you. So don't hesitate to share your thoughts with us on Twitter (**@ubuntu_unity**), Telegram (https://t.me/ubuntuunitydiscuss), and Discord (https://discord.gg/JZryxUMGZf). We're all ears!